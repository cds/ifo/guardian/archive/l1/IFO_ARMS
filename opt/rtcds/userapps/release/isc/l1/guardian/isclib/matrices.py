import cdsutils

##################################################
# LSC/ASC input and output matrices              #
##################################################

# LSC input for the majority
lsc_input = cdsutils.CDSMatrix(
    'LSC-PD_DOF_MTRX',
    ramping=True,
    cols={'POP_A_RF9_I': 1,
          'POP_A_RF9_Q': 2,
          'POP_A_RF45_I': 3,
          'POP_A_RF45_Q': 4,
          'REFL_A_RF9_I': 5,
          'REFL_A_RF9_Q': 6,
          'REFL_A_RF45_I': 7,
          'REFL_A_RF45_Q': 8,
          'POPAIR_A_RF9_I': 9,
          'POPAIR_A_RF9_Q': 10,
          'POPAIR_A_RF45_I': 11,
          'POPAIR_A_RF45_Q': 12,
          'REFLAIR_A_RF9_I': 13,
          'REFLAIR_A_RF9_Q': 14,
          'REFLAIR_A_RF45_I': 15,
          'REFLAIR_A_RF45_Q': 16,
          'REFLAIR_B_RF27_I': 17,
          'REFLAIR_B_RF27_Q': 18,
          'REFLAIR_B_RF135_I': 19,
          'REFLAIR_B_RF135_Q': 20,
          'TRX': 21,
          'TRY': 22,
          'REFLSERVO_SLOW': 23,
          'ALS_COMM': 24,
          'ASAIR_ALF': 25,
          'TR_CARM': 26,
          'TR_REFL9': 27,
          'REFL_DC': 28,
          'OMC_DC':29,
          'ASVAC_A_LF':30,
          'ASVAC_B_LF':31,
          'ASVAC_A_RF45_I':32,
          'ASVAC_A_RF45_Q':33,
          },
    rows={'DARM': 1,
          'CARM': 2,
          'MICH': 3,
          'PRCL': 4,
          'SRCL': 5,
          'MCL':  6,
          'XARM': 7,
          'YARM': 8,
          'REFLBIAS': 9,
          }
    )

# LSC input for OMC/AS45 to DARM and CARM
lsc_input_arm = cdsutils.CDSMatrix(
    'LSC-ARM_INPUT_MTRX',
    ramping=True,
    cols={'OMC_DC': 1,
          'ASVAC_A_LF': 2,
          'ASVAC_B_LF': 3,
          'ALS_DIFF': 4,
          'REFL_CTRL': 5,
          'ASVAC_A_RF45_I': 6,
          'ASVAC_A_RF45_Q': 7
          },
    rows={'DARM': 1,
          'CARM': 2,
          }
    )

# LSC power normalization
lsc_pow_norm = cdsutils.CDSMatrix(
    'LSC-POW_NORM_MTRX',
    cols={'POPAIR_B_RF18_I': 1,
          'POPAIR_B_RF18_Q': 2,
          'POPAIR_B_RF90_I': 3,
          'POPAIR_B_RF90_Q': 4,
          'ASAIR_B_RF18_I': 5,
          'ASAIR_B_RF18_Q': 6,
          'ASAIR_B_RF90_I': 7,
          'ASAIR_B_RF90_Q': 8,
          'REFLAIR_A_LF': 9,
          'REFLAIR_B_LF': 10,
          'POPAIR_A_LF': 11,
          'POPAIR_B_LF': 12,
          'ASAIR_A_LF': 13,
          'ASAIR_B_LF': 14,
          'REFL_A_LF': 15,
          'POP_A_LF': 16,
          'TRX_A_LF': 17,
          'TRY_A_LF': 18,
          },
    rows={'DARM': 1,
          'MICH': 2,
          'PRCL': 3,
          'SRCL': 4,
          'MCL': 5,
          'XARM': 6,
          'YARM': 7,
          'REFLBIAS':8,
          'CARM':9,
          }
    )

# LSC trigger
lsc_trigger = cdsutils.CDSMatrix(
    'LSC-TRIG_MTRX',
    cols={'OMCDC': 1,
          'POPAIR_B_RF18_I': 2,
          'POPAIR_B_RF18_Q': 3,
          'POPAIR_B_RF90_I': 4,
          'POPAIR_B_RF90_Q': 5,
          'ASAIR_B_RF18_I': 6,
          'ASAIR_B_RF18_Q': 7,
          'ASAIR_B_RF90_I': 8,
          'ASAIR_B_RF90_Q': 9,
          'REFLAIR_A_LF': 10,
          'REFLAIR_B_LF': 11,
          'POPAIR_A_LF': 12,
          'POPAIR_B_LF': 13,
          'ASAIR_A_LF': 14,
          'ASAIR_B_LF': 15,
          'REFL_A_LF': 16,
          'POP_A_LF': 17,
          'TRX_A_LF': 18,
          'TRY_A_LF': 19,
          },
    rows={'DARM': 1,
          'MICH': 2,
          'PRCL': 3,
          'SRCL': 4,
          'MCL': 5,
          'XARM': 6,
          'YARM': 7,
          'REFLBIAS': 8,
          'CARM':9,
          }
    )

# LSC output for the majority or DOFs
lsc_output = cdsutils.CDSMatrix(
    'LSC-OUTPUT_MTRX',
    cols={'MICH': 1,
          'PRCL': 2,
          'SRCL': 3,
          'MCL':  4,
          'XARM': 5,
          'YARM': 6,
          'OSC1': 7,
          'OSC2': 8,
          'OSC3': 9,
          'MICHFF': 10,
          'SRCLFF': 11,
          'CPSFF': 12,
          },
    rows={'ETMX': 1,
          'ETMY': 2,
          'ITMX': 3,
          'ITMY': 4,
          'PRM':  5,
          'SRM':  6,
          'BS':   7,
          'PR2':  8,
          'SR2':  9,
          'MC2':  10,
          }
    )

# output arm
lsc_output_arm = cdsutils.CDSMatrix(
    'LSC-ARM_OUTPUT_MTRX',
    cols={'DARM': 1,
          'CARM': 2,
          },
    rows={'ETMX': 1,
          'ETMY': 2,
          'ITMX': 3,
          'ITMY': 4,
          }
    )

######################################################
# ASC Matrices
######################################################

asc_sensors ={
    'AS_A_RF45_I': 1,
    'AS_A_RF45_Q': 2,
    'AS_A_RF36_I': 3,
    'AS_A_RF36_Q': 4,
    'AS_B_RF45_I': 5,
    'AS_B_RF45_Q': 6,
    'AS_B_RF36_I': 7,
    'AS_B_RF36_Q': 8,
    'REFL_A_RF9_I': 9,
    'REFL_A_RF9_Q': 10,
    'REFL_A_RF45_I': 11,
    'REFL_A_RF45_Q': 12,
    'REFL_B_RF9_I': 13,
    'REFL_B_RF9_Q': 14,
    'REFL_B_RF45_I': 15,
    'REFL_B_RF45_Q': 16,
    'REFL_A_DC': 17,
    'REFL_B_DC': 18,
    'AS_A_DC': 19,
    'AS_B_DC': 20,
    'POP_A_DC': 21,
    'POP_B_DC': 22,
    'TRX_A': 23,
    'TRX_B': 24,
    'TRY_A': 25,
    'TRY_B': 26,
    'AS_C_DC': 27,
    'IM4_TRANS': 28,
    'POP_X_RF36_I': 29,
    'POP_X_RF36_Q': 30,
    'POP_X_DC': 31,
    'AS_A_RF72_I':32,
    'AS_A_RF72_Q':33,
    'AS_B_RF72_I':34,
    'AS_B_RF72_Q':35,
    }

asc_dofs = {
    'INP1': 1,
    'INP2': 2,
    'PRC1': 3,
    'PRC2': 4,
    'MICH': 5,
    'SRC1': 6,
    'SRC2': 7,
    'DHARD': 8,
    'DSOFT': 9,
    'CHARD': 10,
    'CSOFT': 11,
    'DC1': 12,
    'DC2': 13,
    'DC3': 14,
    'DC4': 15,
    'DC5': 16,
    'DC6': 17,
    'DC7': 18,
    }

asc_dofs_extra_1 = {
    'CHARD_B': 19,
    'DHARD_B': 20,
    'DSOFT_B': 21,
    'CSOFT_B': 22,
    }

asc_dofs_extra_2 = {
    'ALS_X1': 19,
    'ALS_X2': 20,
    'ALS_X3': 21,
    'ALS_Y1': 22,
    'ALS_Y2': 23,
    'ALS_Y3': 24,
    'ALSX_OSC1': 25,
    'ALSX_OSC2': 26,
    'ALSY_OSC1': 27,
    'ALSY_OSC2': 28,
    'LOCKIN_OSC1': 29,
    }


asc_actuators = {
    'PRM': 1,
    'PR2': 2,
    'PR3': 3,
    'BS': 4,
    'ITMX': 5,
    'ITMY': 6,
    'ETMX': 7,
    'ETMY': 8,
    'SRM': 9,
    'SR2': 10,
    'SR3': 11,
    'IM1': 12,
    'IM2': 13,
    'IM3': 14,
    'IM4': 15,
    'RM1': 16,
    'RM2': 17,
    'OM1': 18,
    'OM2': 19,
    'OM3': 22, # careful for the location of OM3
    'TMSX': 20,
    'TMSY': 21,
    'POP_STEER': 22,
    'OMC_SUS_L': 23,
    'OMC_SUS_T': 24,
    'OMC_SUS_Y': 25,
    }

# Make dofs dictionaries for input and output
asc_dofs_1 = asc_dofs.copy()
asc_dofs_1.update(asc_dofs_extra_1)
asc_dofs_2 = asc_dofs.copy()
asc_dofs_2.update(asc_dofs_extra_2)


# ASC input matrices 
asc_input_pit = cdsutils.CDSMatrix(
    'ASC-INMATRIX_P',
    #ramping=True,
    cols = asc_sensors,
    rows = asc_dofs_1,
    )

asc_input_yaw = cdsutils.CDSMatrix(
    'ASC-INMATRIX_Y',
    #ramping=True,
    cols = asc_sensors,
    rows = asc_dofs_1,
    )


# ASC output matrices
asc_output_pit = cdsutils.CDSMatrix(
    'ASC-OUTMATRIX_P',
    cols=asc_dofs_2,
    rows=asc_actuators,
    )

asc_output_yaw = cdsutils.CDSMatrix(
    'ASC-OUTMATRIX_Y',
    cols=asc_dofs_2,
    rows=asc_actuators,
    )



# asc ads sen matirx
asc_ads_sensors = {
    'DARM_CTRL': 1,
    'PRCL_CTRL': 2,
    'SRCL_CTRL': 3,
    'POPAIR_B_RF18_I': 4,
    'POP_A_LF': 5,
    }

asc_ads_pit_dofs = {
    'PIT1': 1,
    'PIT2': 2,
    'PIT3': 3,
    'PIT4': 4,
    'PIT5': 5,
    'PIT6': 6,
    'PIT7': 7,
    'PIT8': 8,
    'PIT9': 9,
    'PIT10': 10,
    }

asc_ads_yaw_dofs = {
    'YAW1': 1,
    'YAW2': 2,
    'YAW3': 3,
    'YAW4': 4,
    'YAW5': 5,
    'YAW6': 6,
    'YAW7': 7,
    'YAW8': 8,
    'YAW9': 9,
    'YAW10': 10,
    }


asc_ads_input_pit = cdsutils.CDSMatrix(
    'ASC-ADS_PIT_SEN_MTRX',
    cols=asc_ads_sensors,
    rows=asc_ads_pit_dofs,
    )


asc_ads_input_yaw = cdsutils.CDSMatrix(
    'ASC-ADS_YAW_SEN_MTRX',
    cols=asc_ads_sensors,
    rows=asc_ads_yaw_dofs,
    )

# asc ads oscillators matrix
asc_ads_oscillators = {
    'OSC1': 1,
    'OSC2': 2,
    'OSC3': 3,
    'OSC4': 4,
    'OSC5': 5,
    'OSC6': 6,
    'OSC7': 7,
    'OSC8': 8,
    'OSC9': 9,
    'OSC10': 10,
    }

asc_ads_actuators_dither = {
    'PRM': 1,
    'PR2': 2,
    'PR3': 3,
    'BS': 4,
    'ITMX': 5,
    'ITMY': 6,
    'ETMX': 7,
    'ETMY': 8,
    'TMSX': 9,
    'TMSY': 10,
    'SRM': 11,
    'SR2': 12,
    'SR3': 13,
    }

asc_ads_lo_pit = cdsutils.CDSMatrix(
    'ASC-ADS_LO_PIT_MTRX',
    cols=asc_ads_oscillators,
    rows=asc_ads_actuators_dither,
    )

asc_ads_lo_yaw = cdsutils.CDSMatrix(
    'ASC-ADS_LO_YAW_MTRX',
    cols=asc_ads_oscillators,
    rows=asc_ads_actuators_dither,
    )

# asc ads output matrix
asc_ads_dofs_pit = {
    'PIT1': 1,
    'PIT2': 2,
    'PIT3': 3,
    'PIT4': 4,
    'PIT5': 5,
    'PIT6': 6,
    'PIT7': 7,
    'PIT8': 8,
    'PIT9': 9,
    'PIT10': 10,
    }

asc_ads_dofs_yaw = {
    'YAW1': 1,
    'YAW2': 2,
    'YAW3': 3,
    'YAW4': 4,
    'YAW5': 5,
    'YAW6': 6,
    'YAW7': 7,
    'YAW8': 8,
    'YAW9': 9,
    'YAW10': 10,
    }

asc_ads_actuators = {
    'IM3': 1,
    'IM4': 2,
    'PRM': 3,
    'PR2': 4,
    'PR3': 5,
    'BS': 6,
    'ITMX': 7,
    'ITMY': 8,
    'ETMX': 9,
    'ETMY': 10,
    'SRM': 11,
    'SR2': 12,
    'SR3': 13,
    }


asc_ads_output_pit = cdsutils.CDSMatrix(
    'ASC-ADS_OUT_PIT_MTRX',
    cols=asc_ads_dofs_pit,
    rows=asc_ads_actuators,
    )

asc_ads_output_yaw = cdsutils.CDSMatrix(
    'ASC-ADS_OUT_YAW_MTRX',
    cols=asc_ads_dofs_yaw,
    rows=asc_ads_actuators,
    )






