# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
from guardian import GuardState, GuardStateDecorator
from guardian.ligopath import userapps_path
from subprocess import Popen, PIPE,check_output, call
import cdsutils
#from collections import namedtuple
import time
import math
import sys
import runningAvg

sys.path.append('/ligo/cdscfg')
#import stdenv as cds;

sys.path.append('/opt/rtcds/userapps/release/isc/l1/guardian')
import isclib.matrices as matrix

#cds.INIT_ENV()

##################################################
# SYSTEM PARAMETERS
# should be located in:
# sys/<ifo>/guardian/ifoconfig.py
import ifoconfig

##################################################

from ifolib import down

# The down scripts to be run in case of lock loss
#down_script_file = cds.USERAPPS + '/lsc/l1/scripts/autolock/drfpmi/l1_down'
#yarm_down_script_file = cds.USERAPPS + '/lsc/l1/scripts/autolock/drfpmi/l1_yarm_down.sh'

# the other scripts

USERAPPS = '/opt/rtcds/userapps/release/'
ifo = 'l1'
clear_oaf_script_file = '/home/anamaria.effler/scripts/oaf_clear_hist.py'
psl_power_down_script_file = USERAPPS + '/lsc/' + ifo + '/scripts/transition/als/pslpowerdown.py'
aligner_script_file = USERAPPS + '/sus/common/scripts/aligner.py'


class myProc:
    def __init__(self,na,exit_c,fil):
        self.name = na
        self.exit_con = exit_c
        self.file = fil
        self.proc = None

# This list is all the processes/scripts described above.  The states use the names below to call, kill,
# and cleanup the scripts/processes.  
# The down (down, yarm_down) scripts are special and should never be killed, so do not appear in the process list

process_list = {
'clear_oaf' : myProc('clear_oaf_script',None,clear_oaf_script_file),
'psl_power_down' : myProc('psl_power_down_script',None,psl_power_down_script_file),
'aligner' : myProc('aligner_script',None,aligner_script_file)
}


##################################################


# check for some error conditions
def MC_in_fault():
    # currently take as input the state node object, which is for
    # controlling other guardian nodes as a manager

    # currently these faults are only checked in acquire and in the
    # fault state, if the MC is not already locked.  This is intended
    # to help a user diagnose problems if the MC is not locking, but
    # not to interefere with the MC lock needlessly by taking it out
    # of the locked state when it is still locked
    message = []
    flag = False

    # a hierarchy of conditions to check if there is light on the refl
    # PD (only give user most relevant fault message)
    if ezca['PSL-PMC_LOCK_ON'] != -30000:
        message.append("PMC unlocked")
        flag = True
    elif ezca['PSL-PERISCOPE_A_DC_ERROR_FLAG'] != 1:
        message.append("PSL periscope PD error (low light?)")
        flag = True
    elif ezca['IMC-REFL_DC_OUTPUT'] < ifoconfig.imc_refl_no_light_threshold:
        message.append("PSL REFL PD no light (check PZT/MC1 alignment)")
        flag = True

    # check if FSS is locked
    if ezca['PSL-FSS_AUTOLOCK_STATE'] != 4:
        message.append("FSS unlocked")
        flag = True

    # check HAM 2/3 ISI watchdogs
    # FIXME: this should look at ISI guardian nodes
    if ezca['ISI-HAM2_WD_MON_STATE_INMON'] != 1:
        message.append("HAM2 ISI tripped")
        flag = True
    if ezca['ISI-HAM3_WD_MON_STATE_INMON'] != 1:
        message.append("HAM3 ISI tripped")
        flag = True

    # TODO: check for MC2 trips here
    

    # if we get a flag, notify the user, otherwise clear any
    # notifications
    if flag:
        notify(', '.join(message))
    else:
        notify()

    return flag

# return True if requested bit is set
def bitget(i,bit):
    if i == 0:
        return False
    if bit==0:
        return i & 1 == 1
    for z in range(bit):
        i >>= 1
    return i & 1 == 1

# When the DARM loses lock, the integrators in the servo immediately run away and hit the 
# limiters.  This is a pretty good indicator of the DARM losing lock.
def DARM_railed():
    return  abs(ezca['SUS-ETMY_L1_LOCK_L_OUT16']) == ezca['SUS-ETMY_L1_LOCK_L_LIMIT']

# check for lock
def MC_is_locked():
    return ezca['IMC-IMC_TRIGGER_INMON'] >= 35 


def MC_locked_confirm():
    a_value = cdsutils.avg(4, 'IMC-IMC_TRIGGER_INMON')
    if a_value > 85.0:
        return True
    else:
        return False

def XARM_locked():
    return ezca['LSC-TR_X_NORM_INMON'] >= ifoconfig.arm_locked_threshold

def YARM_locked():
    return ezca['LSC-TR_Y_NORM_INMON'] >= ifoconfig.arm_locked_threshold




# These functions are for calling the subscripts.  The kill() command doesn't work on cdsustils.servo 
# commands called from within a bash script.  Servo commands should thus be called as daemon threads in
# a python script, then they can be killed cleanly.  

def startScript(script_name):
    global process_list
    process_list[script_name].exit_con = None
    process_list[script_name].proc = Popen(process_list[script_name].file)
    process_list[script_name].exit_con = process_list[script_name].proc.poll()

def scriptRunning(script_name):
    global process_list
    process_list[script_name].exit_con = process_list[script_name].proc.poll()
    return process_list[script_name].exit_con == None

def exitCondition(script_name):
    global process_list
    return  process_list[script_name].exit_con

def cleanupScript(script_name):
    global process_list
    process_list[script_name].proc = None
    process_list[script_name].exit_con = None    
    

def killScript(script_name):
    global process_list
    if process_list[script_name].proc != None:
                try:
                    # Terminate and remove from the register of living processes
                    process_list[script_name].proc.kill()
                    cleanupScript(script_name)
                    print 'killed ' + script_name
                except:
                    print 'Tried to kill ' + script_name + ' which was not actually running'

def killAllScripts():
    global process_list
    for i, v in enumerate(process_list.keys()):
        killScript(v)

def ifoBurt(file_name):
    global process_list

    # build the burt command
    ifo = IFO.lower()
    snap = userapps_path('isc', ifo, 'burtfiles', file_name)
    log('BURT RESTORE: ' + snap)

    # run burtwb (this is blocking, but quick)
    call(['burtwb', '-f', snap])


#################################################
# DECORATORS
#################################################

class assert_mc_locked(GuardStateDecorator):
    def pre_exec(self):
        if not MC_is_locked():
            return 'DOWN'

class assert_DARM(GuardStateDecorator):
    def pre_exec(self):
        if DARM_railed():
            return 'DARM_DOWN'

##################################################
# NODES
##################################################

# nodes = NodeManager([
#         'IFO_IMC',
#         'SUS_MC2',
#         'SUS_MC3',
#         ])

##################################################
# STATES
##################################################

# initial request on initialization
request = 'IDLE'

class IDLE(GuardState):
    goto = True

    def main(self):
        # Kill all the running scripts
        killAllScripts()
 
    def run(self):
        if MC_in_fault():
            return 'MC_FAULT'
        
        if MC_locked_confirm():
            return True

        notify('MC not locked: cannot leave IDLE state')


class INIT(GuardState):
    def run(self):
        if MC_in_fault():
            return 'MC_FAULT'
        return True


# wait for all faults to be cleared, the jump to IDLE to find state
class MC_FAULT(GuardState):
    request = False

    def run(self):
        if MC_in_fault():
            return
        return 'IDLE'


# This is for an algorithm failure, when the algorithm gets confused.  
class LOCK_FAULT(GuardState):
    request = False

    def run(self):
        notify("LOCK FAULT: check ALS status")
        # Just sit here until a user requests one of the "goto" states: IDLE or DOWN
        return 


class LOCKLOSS(GuardState):
    """Record lockloss event"""
    request = False
    def main(self):
        return True


# reset everything to the good values for acquistion.  These values are stored in the down script.
class DOWN(GuardState):
    goto = True

    def main(self):        
        # Kill all the running scripts
        killAllScripts()

        # EXECUTE THE MAIN DOWN CODE
        # down.reset_all(ezca, log)

       # reduce PSL Power to 2W
        # startScript('psl_power_down')

        # restore LSC and ASC down settings
        #ifoBurt('lsc_down.snap')
        #ifoBurt('asc_down.snap')
        #ifoBurt('isc_rset.snap')

        # restore SUS down settings
        #ifoBurt('sus_down.snap')
        #ifoBurt('sus_rset.snap')
        
        #ezca.switch('SUS-BS_M2_OLDAMP_P','FM1','INPUT','OUTPUT','ON')
         
    def run(self):
        if MC_in_fault():
            return 'MC_FAULT'

        if MC_locked_confirm():
            return True

class RELOCK_MC(GuardState):
    request = False

    def main(self):
        if MC_in_fault():
            return 'MC_FAULT'
        
        # The mode cleaner has unlocked during the initial comm/diff steps, we turn off the
        # output from XARM and reset the MCL gain.   

        ezca.switch('LSC-XARM','OUTPUT','OFF')
        ezca['LSC-XARM_RSET'] = 2
        # TODO: once the IMC Guardian is fixed to tdo this, can remove the next line
        ezca['LSC-MC_GAIN'] = -125
        ezca['IMC-VCO_TUNEOFS'] = 0

    def run(self):
        if MC_locked_confirm():
            return True

############################################
# ALS COMM

#TODO: Need to transition to DOWN without killing the IMC

class SET_ALS_COMM(GuardState):
    request = False
    def main(self):
        #TODO: matrix elements
        ezca.switch('LSC-XARM','FMALL','OUTPUT','OFF')
        ezca['LSC-XARM_TRAMP'] = 0        
        ezca['LSC-XARM_GAIN'] = 0
        ezca['LSC-XARM_TRAMP'] = 3
        ezca['LSC-MC_TRAMP'] = 5
        ezca.switch('LSC-XARM','FM4','FM5','OUTPUT','ON')       

class ALSX_PDH_CHECK(GuardState):
    request = False
    def main(self):
        xpdh_control = ezca['ALS-X_REFL_SERVO_FASTMON']
    def run(self):        
        if abs(xpdh_control-0.8) > 1.5:
            e.write('ALS-X_REFL_SERVO_IN1EN',0)
            e.write('ALS-X_REFL_SERVO_COMBOOST',0)
            e.write('ALS-X_REFL_SERVO_COMCOMP',0)
            time.sleep(1)
            e.write('ALS-X_REFL_SERVO_IN1EN',1)
            e.write('ALS-X_REFL_SERVO_COMCOMP',1)
            e.write('ALS-X_REFL_SERVO_COMBOOST',1)
            time.sleep(1)
            xpdh_control = e.read('ALS-X_REFL_SERVO_FASTMON')
            return
        else:
            return True

        
        #if PDH control signal to large, unlock PDH and relock.

#class TUNE_IMC_VCO(GuardState):
##class COMM_PLL_ENGAGE(GuardState):
#    # Tune the IMC VCO offset to bring the beat-note within range of the Comm VCO, before engaging the PLL
#    request = False
#    @assert_mc_locked
#    def main(self):
#    @assert_mc_locked
#    def run(self):            
#        return True

class ACQ_ALS_COMM(GuardState):
    request = False
    @assert_mc_locked
    def main(self):
        comm_pll_err = ezca['ALS-C-COMM_PLL_ERRMON']
    @assert_mc_locked
    def run(self): 
        if abs(comm_pll_err) > 0.15:
            imcvco = ezca['IMC-VCO_TUNEOFS']
            x = math.copysign(0.03,ezca['ALS-C_COMM_PLL_CTRLMON'])
            ezca['IMC-VCO_TUNEOFS'] = imcvco+x
            time.sleep(0.1)
            comm_pll_err = ezca['ALS-C-COMM_PLL_ERRMON']       
            return
        else:
            ezca['LSC-XARM_GAIN'] = -3
            ezca['LSC-MC_GAIN'] = 0
            time.sleep(3)
            ezca.switch('LSC-XARM','FM10','ON')
            time.sleep(1)
            ezca.switch('LSC-XARM','FM6','FM3','ON')
            time.sleep(0.3)            
            return True

class ALS_COMM(GuardState):
    @assert_mc_locked
    def run(self):
        return True

############################################
# ALS DIFF

class DARM_DOWN(GuardState):
    request = False
    @assert_mc_locked
    def main(self):
        killAllScripts()
        # EXECUTE THE MAIN DARM DOWN CODE
        down.reset_darm(ezca, log)
        return True

class SET_ALS_DIFF(GuardState):
    request = False
    @assert_mc_locked
    def main(self):
        ezca['LSC-DARM_TRAMP'] = 0
        ezca['LSC-DARM_GAIN'] = 0
        time.sleep(1)
        ezca['LSC-DARM_TRAMP'] = 30
        ezca.switch('LSC-DARM','FM1','FM4','FM10','OFF')
        ezca.switch('LSC-DARM','FM2','FM3','FM5','FM6','FM7','FM8','FM9','INPUT','OUTPUT','ON')
        time.sleep(0.3)
        ezca['LSC-DARM_RSET'] = 2

class ALSY_PDH_CHECK(GuardState):
    request = False
    @assert_mc_locked
    def main(self):
        ypdh_control = ezca['ALS-Y_REFL_SERVO_FASTMON']
    @assert_mc_locked
    def run(self):
        if abs(ypdh_control-0.8) > 1.5:
            e.write('ALS-Y_REFL_SERVO_IN1EN',0)
            e.write('ALS-Y_REFL_SERVO_COMBOOST',0)
            e.write('ALS-Y_REFL_SERVO_COMCOMP',0)
            time.sleep(1)
            e.write('ALS-Y_REFL_SERVO_IN1EN',1)
            e.write('ALS-Y_REFL_SERVO_COMCOMP',1)
            e.write('ALS-Y_REFL_SERVO_COMBOOST',1)
            time.sleep(1)
            ypdh_control = e.read('ALS-Y_REFL_SERVO_FASTMON')
            return
        else:
            return True


class DIFF_BEATNOTE_CHECK(GuardState):
    request = False
    @assert_mc_locked
    def main(self):
        log('State: DIFF_BEATNOTE_CHECK')
        if ezca['ALS-C_DIFF_A_DEMOD_RFMON'] < ifoconfig.diff_beatnote_threshold:
            log('als diff beatnote too low')
    @assert_mc_locked
    def run(self):
        if not is_diff_beat_good():
            notify("ALS beatnote below threshold")
            return False
        return True


class ACQ_ALS_DIFF(GuardState):
    request = False
    #TODO: This needs something checking DARM while the gain ramps
    @assert_mc_locked
    def main(self):
        # Slowly Ramp DARM Gain        
        ezca['LSC-DARM_GAIN'] = -0.003
        time.sleep(20)
        ezca['LSC-DARM_TRAMP'] = 15
        time.sleep(1)
        ezca['LSC-DARM_GAIN'] = -0.02
        time.sleep(10)
        # Increase Diff PLL Gain
        ezca['ALS-C_DIFF_PLL_COMP1'] = 1
        ezca['ALS-C_DIFF_PLL_GAIN'] = -5
        # Increase DARM Gain        
        ezca['LSC-DARM_TRAMP'] = 5
        ezca['LSC_DARM_GAIN'] = -0.4
        time.sleep(5)
        #Turn on integrator
        ezca.switch('LSC-DARM','FM10','ON')
        # Feedback to TOP mass
        ezca['SUS-ETMY_M0_LOCK_L_TRAMP'] = 45
        ezca['SUS-ETMY_M0_LOCK_L_RSET'] = 2
        time.sleep(0.3)
        ezca['SUS-ETMY_M0_LOCK_L_GAIN'] = 0.1

class ALS_DIFF(GuardState):
    @assert_mc_locked
    @assert_DARM 
    def run(self):
        return True

##################################################
# X(Y)ARM IR LOCKED

class ARM_IR_SET(GuardState):
    request = False
    def main(self):
        ezca['LSC-YARM_TRAMP'] = 0
        matrix.lsc_trigger.zero(row='YARM')
        matrix.lsc_input.zero(row='YARM')
        time.sleep(1)
        #ezca['LSC-OUTPUT_MTRX_LOAD_MATRIX'] = 1 #Load what exactly?
        matrix.lsc_input['YARM','ASVAC_A_RF45_I'] = 1
        matrix.lsc_input.TRAMP = 0    
        time.sleep(0.1)
        matrix.lsc_input.load()
        matrix.lsc_pow_norm['YARM', 'TRX_A_LF'] = 0
        matrix.lsc_pow_norm['YARM', 'TRY_A_LF'] = 0

        #Open Fast Shutter
        ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1

        # Set up Trans QPD Paths
        ezca['LSC-TR_DC_OR_QPD_SUM'] = 1
        #TODO: Run zero offset script?
        ezca['LSC-TR_X_QPD_B_SUM_OFFSET'] = 0
        ezca['LSC-TR_Y_QPD_B_SUM_OFFSET'] = 0
        # Filter settings:
        ezca['LSC-YARM_GAIN'] = -3
        ezca.switch('LSC-YARM', 'FM2', 'FM4', 'FM5', 'FM6', 'INPUT', 'ON')

class LOCK_XARM_IR(GuardState):
    request = False
    @assert_mc_locked    
    def main(self):
        # Set triggering      
        matrix.lsc_trigger['YARM', 'TRX_A_LF'] = 1  
        matrix.lsc_trigger['YARM', 'TRY_A_LF'] = 0
        # Turn on output
        ezca.switch('LSC-YARM', 'OUTPUT', 'ON')

        self.avg = runningAvg.RunningAvg(ezca,'LSC-TR_X_NORM_INMON',3)

    @assert_mc_locked
    def run(self):
        #a_value = cdsutils.avg(3, 'LSC-TR_X_NORM_INMON') #or LSC-Y_TR_A_LF_OUTMON?
        a_value, done = self.avg.avg()
        if not done:
            return
        if a_value > ifoconfig.arm_locked_threshold:
            return True

class XARM_IR_LOCKED(GuardState):
    @assert_mc_locked
    def run(self):
        #TODO: xarm is locked checker. Use TRPD
        return True

class LOCK_YARM_IR(GuardState):
    request = False
    @assert_mc_locked
    def main(self):
        # Set triggering
        matrix.lsc_trigger['YARM', 'TRX_A_LF'] = 0  
        matrix.lsc_trigger['YARM', 'TRY_A_LF'] = 1        
        # Turn on output
        ezca.switch('LSC-YARM','OUTPUT', 'ON')
        
        self.avg = runningAvg.RunningAvg(ezca,'LSC-TR_Y_NORM_INMON',3)


    @assert_mc_locked
    def run(self):
        #a_value = cdsutils.avg(3, 'LSC-TR_Y_NORM_INMON') #or LSC-Y_TR_A_LF_OUTMON?
        a_value, done = self.avg.avg()
        if not done:
            return
        if a_value > ifoconfig.arm_locked_threshold:
            return True

class YARM_IR_LOCKED(GuardState):
    @assert_mc_locked    
    def run(self):
        #TODO: yarm is locked checker. Use TRPD
        return True

class UNLOCK_ARM_IR(GuardState):
    request = False
    def main(self):
        ezca['LSC-YARM_TRIG_THRESH_OFF'] = -10
        ezca['LSC-YARM_FM_TRIG_THRESH_OFF'] = -10
        ezca['LSC-YARM_TRAMP'] = 3
        time.sleep(1)
        ezca['LSC-YARM_GAIN'] = 0
        self.timer['t_arm_unlock'] = 4

    def run(self):
        if self.timer['t_arm_unlock']:
            ezca.switch('LSC-YARM', 'OUTPUT', 'OFF')
            ezca['LSC-YARM_TRIG_THRESH_OFF'] = 3
            ezca['LSC-YARM_FM_TRIG_THRESH_OFF'] = 3
            #Open Fast Shutter
            ezca['SYS-MOTION_C_FASTSHUTTER_A_BLOCK'] = 1
            return True

#TODO: Transition smoothly back to down state

##################################################

edges = [
    ('INIT','IDLE'),
    ('IDLE','DOWN'),
#ALS EDGES
    ('DOWN', 'ALSX_PDH_CHECK'),
    ('ALSX_PDH_CHECK','SET_ALS_COMM'),
    ('SET_ALS_COMM','ACQ_ALS_COMM'),
    ('ACQ_ALS_COMM','ALS_COMM'),
    ('ALS_COMM','ALSY_PDH_CHECK'),
    ('DARM_DOWN','ALSY_PDH_CHECK'),
    ('ALSY_PDH_CHECK','SET_ALS_DIFF'),
    ('SET_ALS_DIFF','DIFF_BEATNOTE_CHECK'),
    ('DIFF_BEATNOTE_CHECK','ACQ_ALS_DIFF'),
    ('ACQ_ALS_DIFF','ALS_DIFF'),
    ('DOWN','ARM_IR_SET'),
    ('ARM_IR_SET','LOCK_XARM_IR'),
    ('LOCK_XARM_IR','XARM_IR_LOCKED'),
    ('ARM_IR_SET','LOCK_YARM_IR'),
    ('LOCK_YARM_IR','YARM_IR_LOCKED'),
    ('XARM_IR_LOCKED','UNLOCK_ARM_IR'),
    ('YARM_IR_LOCKED','UNLOCK_ARM_IR'),
    ('UNLOCK_ARM_IR','DOWN')
    ]

##################################################
# SVN $Id$
# $HeadURL$
             
            
