##################################################
# aLIGO IFO PARAMETERS FILE
##################################################


##################################################
# LOCK AQUISITION 
# acqsus is the ETM ESD with which we lock
acqsus = 'ETMX'

# Darm actuator is the ETM for which we feed back to the higher stages (M0,L1,L2)
darm_actuator = 'ETMX'	#Nominally ETMY, Options ETMX, ETMY, BOTH 


# Set this flag to 1 if you want to use same actuator in lock acq and low noise. Otherwise set to zero. 
one_esd_flag = 0 

# ETMY BIAS SIGN FLIPPING
sign_flip = 1

##################################################
# INPUT

##################################################

# threshold
imc_refl_no_light_threshold = 15

##################################################
# FULL LOCK
# set the 'final' power 
power45 = 25

# 2000 is for 1W input.  It's scaled where it's used in the code
drmi_locked_threshold_pop18i = 800 #was 4000 before the ND was added 20181116

# CARM OFFSETs
#carm_offset_diff_servo   = -7.5	#No longer use AJM 20161129
#carm_offset_qpds_engage  = -12.0	#No longer use AJM 20161129
carm_offset_asq          = -18
carm_offset_dhard        = -90
carm_offset_refl9        = -150
carm_offset_nudge2zero   = -200

engage_slow_wfs_wait     = 3


diff_beatnote_threshold = -31	#AE 180925 removed 25dB amplifier #-12 AM changed 160808 (new DIFF BBPD amplification)



# Calibration lines
#Modified JCB 2016/11/15, new lower frequency lines; Lines Values put back by shivaraj on 10/28/2016 4:00 CST
#JCB 2019/01/15, new line scheme L1 = line 1, L2 = line 2, L3 = line 3
#UIM line
calibration_line1_freq = 15.1 #22.7#17.8 #33.7
calibration_line1_clkgain = 30#0.24#0.08#0.33#1 #0.003 #0.007
#PUM line
calibration_line2_freq = 15.7 #22.7#17.8 #33.7
calibration_line2_clkgain = 0.08#0.24#0.08#0.33#1 #0.003 #0.007
#ESD only line
calibration_line3_freq = 16.9 #23.9#20.5 #35.3 
calibration_line3_clkgain = 0.150#0.052#0.026#0.08 #0.0005 


etmy_lkin_p_freq = 35.7
etmy_lkin_p_clkgain = 0.0117
etmy_lkin_y_freq = 538.7
etmy_lkin_y_clkgain = 0.9703

# Pcal lines
pcalx_line1_freq = 0
pcalx_line1_amp = 0#31170.0
pcaly_line1_freq = 16.3 #23.3#18.3 #34.7
pcaly_line1_amp = 1200#400#200 #20 #6.5
pcaly_line2_freq = 434.9
pcaly_line2_amp = 7200 #613.0 #Going to need to be tweaked up
pcaly_line3_freq = 1083.1
pcaly_line3_amp = 16160.0

pcaly_line1_phase = -0.358154
pcaly_line2_phase = -9.55591#Needs to be updated (absorbed into the kappa epics phase calculation) 
pcaly_line3_phase = -23.7986

#Do we use pcalx or pcaly for tdep?
use_pcaly = 1

###################################################
# Single Arm Lock

arm_locked_threshold = 15

###################################################
# ADS settings

# quad dither settings
#itmx_p_freq = 8.3 #AE changed too much noise at 6.75 # MK 02/10/18 19.1  # Does not seem necessary, leave to SDF, CB AE, 20190130
#itmy_p_freq = 7.05 # MK 02/10/18 19.65 
#etmx_p_freq = 7.40 # MK 02/10/18 7.1 #6.7 #7.2	#7.4
#etmy_p_freq = 7.65 # MK 02/10/18 7.7 #7.4 #8.4	#7.95
#itmx_y_freq = 7.95 # MK 02/10/18 21.3  
#itmy_y_freq = 8.30 # MK 02/10/18 21.85
#etmx_y_freq = 8.61 # MK 02/10/18 8.1 #11.2	#8.3 
#etmy_y_freq = 8.90 # MK 02/10/18 8.8 #12.4	#8.65
quad_clk_gain = 300	#100 Increased AJM20190207	#Reduce to 30
quad_sin_gain = 1	#Increase to 3
quad_cos_gain = 1	#Increase to 3

# pr dither settings
pr2_p_freq = 17.85	#17.25	#too close to BS bounce mode
pr2_y_freq = 19.65
pr3_p_freq = 13.15
pr3_y_freq = 14.85
pr_clk_gain = 0.4
pr_sin_gain = 1
pr_cos_gain = 1

#prm dither settings
prm_p_freq = 11.1	#17.25	#too close to BS bounce mode
prm_y_freq = 12.7
prm_clk_gain = 300
prm_sin_gain = 1
prm_cos_gain = 1

# sr dither settings
sr2_p_freq = 10.63		#16.15
sr2_y_freq = 11.97		#18.45
#sr3_p_freq = 12.25
#sr3_y_freq = 14.15
sr_clk_gain = 1
sr_sin_gain = 1
sr_cos_gain = 1

# demod phases
pit1_ph = 25
pit2_ph = -150
pit3_ph = 20   # MK 02/10/18  30 #-30 HY 08/28/2016
pit4_ph = -165 # MK 02/10/18 -30
yaw1_ph = 0
yaw2_ph = -180
yaw3_ph = -45  # MK 02/10/18 -30#-16 HY 08/28/2016
yaw4_ph = -25  # MK 02/10/18 -95#-30 HY 08/28/2016
pit5_ph = -40
yaw5_ph = -25

suspensions = ['SUS_RM1',
    'SUS_RM2',
    'SUS_IM1',
    'SUS_IM2',
    'SUS_IM3',
    'SUS_IM4',
    'SUS_MC1',
    'SUS_MC3',
    'SUS_PR3',
    'SUS_PRM',
    'SUS_MC2',
    'SUS_PR2',
    'SUS_BS',
    'SUS_ITMY',
    'SUS_ITMX',
    'SUS_SR2',
    'SUS_SRM',
    'SUS_SR3',
    'SUS_ETMY',
    'SUS_ETMX',
    'SUS_TMSX',
    'SUS_TMSY',
    'SUS_OM1',
    'SUS_OM2',
    'SUS_OM3',
    'SUS_OMC']

seismics = ['HPI_HAM1',
    'SEI_HAM2',
    'SEI_HAM3',
    'SEI_HAM4',
    'SEI_HAM5',
    'SEI_HAM6',
    'SEI_BS',
    'SEI_ITMX',
    'SEI_ITMY',
    'SEI_ETMX',
    'SEI_ETMY']



