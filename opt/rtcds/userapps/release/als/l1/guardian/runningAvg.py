import time

EPICS_RATE = 0.0625 #1/16.0

class RunningAvg(object):

    def __init__(self,ezca,channel,duration):
        self._ezca = ezca
        self._done = False
        if not(type(channel) == list):
            channel = [channel]
        self._channel = channel
        self._duration = duration
        self._bufferSize = int(duration*16)
        self._circBuffer = []
        for x in range(0,len(channel)):
            self._circBuffer.append([0]*self._bufferSize)
        self._bufferPointer = 0
        self._lastTime = time.time()

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "%s(%s, '%s', '%s)" % (self.__class__.__name__,self._ezca,self._channel,self._duration)

    def avg(self):
        if self._bufferPointer >= self._bufferSize:
            self._bufferPointer = 0
            self._done = True
        if time.time() - self._lastTime >= EPICS_RATE:
            for x in range(0,len(self._channel)):
                self._circBuffer[x][self._bufferPointer] = float(self._ezca[self._channel[x]])
            self._lastTime = time.time()
            self._bufferPointer = self._bufferPointer + 1
        return_array = []
        for x in range(0,len(self._channel)):
            return_array.append(sum(self._circBuffer[x])/float(self._bufferSize))
        if len(return_array) == 1:
            return_array = return_array[0]
        return return_array, self._done

